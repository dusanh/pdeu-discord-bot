from typing import List, Tuple, Dict
import json
from itertools import islice

SUPPORTED_CURRENCIES = ["SEK", "DKK", "CZK", "GBP", "AUD", "EUR", "BTC"]


def get_conversion_rates(rate_file: str) -> Dict:
    """Loads the json with conversion rates and outputs it as a dictionary."""
    with open(rate_file, "r") as f:
        json_file = f.read()

    # TODO move the logic below to a standalone fn
    # take the json loaded as string and transform it to dictionary before output
    rates_json = json.loads(json_file)
    # add EUR conversion rate which is always 1.0
    rates_json["rates"]["EUR"] = 1.0

    return rates_json["rates"]


def convert_to_list(text: str) -> List[str]:
    """Gets the message, creates a list of individual words."""
    textlist = text.split(" ")
    return textlist


def get_abbreviations(textlist: List[str]) -> List[Tuple[str, str]]:
    """Goes over the list and returns a list of the amount-currency pairs."""
    pairs = []
    for index, word in enumerate(textlist):
        # get the word preceding the abbreviation
        preceding = textlist[index - 1]
        if check_if_currency(word):
            # ignores unsupported currencies and creates a list of supported amount-currency pairs
            pairs.append((preceding, word))

    if pairs:
        print("Currency pairs: {0}".format(pairs))

    return pairs


def check_if_currency(currency: str) -> bool:
    """Checks if the currency is supported."""
    if currency in SUPPORTED_CURRENCIES:
        return True
    else:
        return False


def validate(pair: Tuple[str, str]) -> bool:
    """Runs various validation function over the amount-currency pair.
    Each function returns false if validation fails. Ultimately returns True only when all individual functions pass.
    """
    # unpack the amount-currency tuple
    amount, currency = pair

    def check_if_number(amount: str) -> bool:
        """Checks if the amount is actually a number."""
        if str(amount).replace('.', '', 1).isdigit():
            return True
        else:
            return False

    def check_if_not_large(amount: str) -> bool:
        """Checks if the amount is not too large."""
        if float(amount) < 100000000:
            return True
        else:
            return False

    # validate the pair through the individual functions and only return True if all pass
    if check_if_number(amount) and check_if_not_large(amount):
        return True
    else:
        print("Validation failed for pair: {0}".format(pair))
        return False


def convert_currency(pair: Tuple[str, str], conversion_rates: Dict) -> Dict:
    """Convert the amount in the pair to other supported currencies based on the supplied currency rate."""
    # convert by multiplying the original amount by the conversion rate to EUR
    in_eur = float(pair[0]) / conversion_rates[pair[1]]

    # change values in the copy but keep the original for the next iteration
    converted_currency = conversion_rates.copy()
    for currency in conversion_rates:
        converted_currency[currency] = in_eur * conversion_rates[currency]

    # add original currency pair and remove the currency rate for the original
    converted_currency["orig_currency"] = pair
    converted_currency.pop(pair[1])

    return converted_currency


def convert_from_message(message: str) -> List[Dict]:
    """Wraps the logic for filtering and conversion of the currencies in a message."""
    message = message.replace("\n", " ").strip()
    conversion_rates = get_conversion_rates("fixerio/forex.json")
    pairs = get_abbreviations(convert_to_list(message))

    output = []
    for pair in islice(pairs, 0, 5):  # hard limit on number of pairs set to 5
        if validate(pair):
            converted = convert_currency(pair, conversion_rates)
            output.append(converted)

    return output
