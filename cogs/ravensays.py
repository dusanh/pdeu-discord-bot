#!/usr/bin/env python3

"""
This is the command that prints a Markov Chain generated message using Ravenholdt's
chat history
"""

import discord
from discord.ext import commands
import ravensays.generate_model


class Ravensays(commands.Cog):

    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def ravensays(self, ctx):
        """Generates a message Ravenholdt would say anyway at some point."""
        await ctx.send(ravensays.generate_model.generate_sentence())


def setup(bot):
    bot.add_cog(Ravensays(bot))
    print("Cog loaded: Ravensays")
