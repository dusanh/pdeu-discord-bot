#!/bin/bash

mkdir -p $HOME/.config/systemd/user
cp fixerio.service $HOME/.config/systemd/user
cp fixerio.timer $HOME/.config/systemd/user
systemctl --user daemon-reload
systemctl --user start fixerio.service
systemctl --user enable fixerio.timer
