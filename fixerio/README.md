# Fixer.io for PDEU

This allows us to access the Fixer.io API, and daily update the currency exchange rate for the common currencies our users use.

## Installing

Make sure that the folder in ```fixerio.service``` matches the current folder.

```
[Service]
ExecStart=/usr/bin/python %h/pdeubot/fixerio/forex.py
```

Put your Fixer.io API access key in ```fixerio/accesskey```.

Run the installation script.
```
./fixerioinstall.sh
```

This will install the necessary pip package for accessing the Fixer.io API, create and copy user level Systemd unit files into the relevant directory.
