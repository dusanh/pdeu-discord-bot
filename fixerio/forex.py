import urllib3
import json
import os


def retrieve_accesskey():
    # First, obtain the absolute path to the file
    abs_file_path = retrieve_absfilepath('fixerio_key')

    # Open file and read the first line
    with open(abs_file_path, 'r') as accesskeyfile:
        key = accesskeyfile.readline().strip()

    return key


def retrieve_absfilepath(filename):
    abs_path = os.path.dirname(__file__)
    abs_filepath = os.path.join(abs_path, filename)

    return abs_filepath


def get_rates(accesskey):
    url = "http://data.fixer.io/api/latest?access_key=" + accesskey + "&symbols=SEK,DKK,CZK,GBP,AUD,BTC"

    http = urllib3.PoolManager()
    r = http.request("GET", url)
    rates = json.loads(r.data.decode("utf-8"))

    return rates


output = get_rates(retrieve_accesskey())
abs_file_path = retrieve_absfilepath('forex.json')
with open(abs_file_path, 'w') as outfile:
    json.dump(output, outfile)
