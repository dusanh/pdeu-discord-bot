# PDEU Discord Bot

This is repository for the ```/r/PlayDateEU``` discord server bot. It's built with Python 3 and utilizes the [discord.py library](https://discordpy.readthedocs.io/en/latest/).

If you like to contribute, feel free to contact us here or on the discord server itself [https://discord.gg/8dyKBwx](https://discord.gg/8dyKBwx)

## Usage

Run the bot with ```python3 pdeu-discord-bot.py``` and if you have the bot invited to your server, it should join automatically.

The bot responds to the following chat messages:
```
> I for one welcome our AI overlords.
Very well, you will be killed last, <username>!
```
This is used to test basic functionality. The bot will only respond to the whitelisted username set in ```nice.py```

```
> nice
I eat fish and chips on the bus.
```
If a username defined in ```nice.py``` says "nice" or a derivative, the bot will respond with a random choice from a list of replies.
```
> 12 EUR
12.0 EUR is: 132.62 SEK  89.83 DKK  329.7 CZK  10.76 GBP  21.74 AUD
```
Any message containing the string ```<number> EUR```, or other currency ISO format configured in ```currency.py```, will get converted to other configured currencies using a relevant exchange rate.

The exchange rates are retrived from ```fixerio/forex.json```. This file is either created manually, or re-created daily using ```fixerio/fixerio.service```.

## Installation

### Just running the bot

* Checkout the repository.
* Install the needed discord.py library with ```python3 -m pip install -U discord.py```
* _See ```requirements.txt``` for any other dependencies you might be missing if you run into problems. (These should already be installed on your system by default though.)_
* Obtain Discord access token and put it in a file named ```token``` inside the root of the repository folder.
* _Optional: If you want to use the fixer.io API for the currency conversion extension, you will also need to obtain their API key and put it into ```fixerio/fixerio_key```._
* Run the bot and invite it to your server. Read [Discord's docs](https://discord.com/developers/docs/topics/oauth2#bots) for more details on how to that.

### Development

* Install packages needed to build project dependencies with ```sudo apt install gcc python3.7-dev```
* Checkout the repository.
* Create a new python virtual environment with ```python3 -m venv venv``` and active it with ```source venv/bin/activate```
* Install dependencies into it with ```pip install -r requirements.txt```
* If you also want to run the bot, see the instructions above.

## Structure

The main file ```pdeu-discord-bot.py``` loads individual extensions from the ```cogs/``` folder. Each extension file corresponds to one distinct functionality of the bot.

Other folders are for auxiliary scripts and files and are not used by the bot directly.